package facci.diegoquijije.evaluacionmovil4b;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

public class Resultado extends AppCompatActivity {

    TextView texto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resultado);

        texto = (TextView) findViewById(R.id.textpara);
        String bundle = this.getIntent().getExtras().getString("Libras");
        Double gramos = Double.valueOf(bundle)*453.592;
        texto.setText(gramos.toString());

        Log.e("hola","conversion finalizada");

    }
}
