package facci.diegoquijije.evaluacionmovil4b;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;


public class MainActivity extends AppCompatActivity {

    EditText txtconvertirLibras;
    Button btnConvertirAGramos;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtconvertirLibras = (EditText) findViewById(R.id.txtconvertirLibras);
        btnConvertirAGramos = (Button) findViewById(R.id.btnConvertirAGramos);
        btnConvertirAGramos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(MainActivity.this, Resultado.class);
                Bundle bundle = new Bundle();

                bundle.putString("Libras", txtconvertirLibras.getText().toString());

                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
    }
}
